var express = require('express'),
  http = require('http'),
  path = require('path'),
  app = express(),
  basicAuth = require('basic-auth'),
  io = require('socket.io'),
  crypto = require('crypto'),
  cat = require('catstream'),
  masterUser = 'sogeti',
  masterPass = 'ngPassword#1',
  port = (process.env.PORT || 3000);

app.use(function (err, req, res, next) {
  console.error(err.stack);
  res.status(500).send('Something broke!');
});

// Static content
app.use('/', express.static(path.join(__dirname, 'public')));

// Authentication
var auth = function (req, res, next) {
  function unauthorized(res) {
    res.set('WWW-Authenticate', 'Basic realm=Authorization Required');
    return res.sendStatus(401);
  }

  var user = basicAuth(req);

  if (!user || !user.name || !user.pass) {
    return unauthorized(res);
  }


  if (user.name === masterUser && user.pass === masterPass) {
    return next();
  } else {
    return unauthorized(res);
  }
};

// Server
app.get('/server/:part', auth, function (req, res) {
  var part = parseInt(req.params.part);
  if (!(part === 1 || part === 2 || part === 3)) {
    res.sendStatus(404);
    return;
  }
  var c = cat();
  res.writeHead(200, {'Content-Type': 'text/html'});
  c.pipe(res);
  c.end([
      'views/header.html',
      'views/part' + part + '.html',
      'views/footer-server.html'
    ].join('\n')
  );
});

// Client
app.get('/part/:part', function (req, res) {
  var part = parseInt(req.params.part);
  if (!(part === 1 || part === 2 || part === 3)) {
    res.sendStatus(404);
    return;
  }
  var c = cat();
  res.writeHead(200, {'Content-Type': 'text/html'});
  c.pipe(res);
  c.end([
      'views/header.html',
      'views/part' + part + '.html',
      'views/footer-client.html'
    ].join('\n')
  );
});

// Get multiplexer token
function createHash(secret) {
  var cipher = crypto.createCipher('blowfish', secret);
  return (cipher.final('hex'));
}

app.get("/token", function (req, res) {
  var ts = new Date().getTime();
  var rand = Math.floor(Math.random() * 9999999);
  var secret = ts.toString() + rand.toString();
  res.send({secret: secret, socketId: createHash(secret)});
});

// Start server
var server = http.createServer(app).listen(port, function () {
  console.log("Express server listening on port " + port);
});

io = io(server);

// Multiplexer
io.on('connection', function (socket) {
  socket.on('multiplex-statechanged', function (data) {
    if (typeof data.secret == 'undefined' || data.secret == null || data.secret === '') return;
    if (createHash(data.secret) === data.socketId) {
      data.secret = null;
      socket.broadcast.emit(data.socketId, data);
    }
  });
});
